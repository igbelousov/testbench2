#!/usr/bin/env python

#
# Requirements:
# It should accept an input file
# specifying details like hostname,
# path, and requests per second, and then generate the requested load until the program
# has been shut down.
# Usage:
# python3 testbench.py --conf=testbench.conf
#
# Example of config file:
# serverURL: 'https://blhaa.myhost.com'
# path: 'mypath'
# targetRPS: 60
# authKey: 'blahblahblah'
#
#
import os
import logging
import logging.handlers
import time
import json
from multiprocessing import Process, TimeoutError
import argparse
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import yaml

#
# Logger
#
mylogger = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)-8s %(module)s.%(funcName)s: %(message)s'))
mylogger.addHandler(stream_handler)


class TestBench:
    def __init__(self):
        self.rpm = None
        self.path = None
        self.host = None
        self.endPoint = None
        self.load = None
        self.headers = {'Content-Type': 'application/json' }
        self.confFile = None
        self.confData = None
        self.min_http_request_timeout_sec = 1
        self.summary_total_reqs = 0
        self.summary_avg_rps = 0.0

        #
        # how much difference in seconds we allow from expected stop
        # to actual test end time per control period interval
        #
        self.flow_control_period_end_time_margin_sec = 1
        #
        # number of parallel workers running load
        # that should be parametirized or auto adjusted depending on workers
        # performance, for high latency requests hard to achieve big RPMs
        # with single thread
        self.max_workers = 2
        # sampilng workers performance every 10 seconds
        # can be used to adjust the load
        self.flow_control_period_sec = 10
        #
        # initial load warmup time
        # when 0 - then all workers starts at the same time
        self.warmup_time = 0
        #
        # workers rate_limiting sleep in seconds
        # when hitting error 429, then sleep this amount of time
        # start with 0
        self.worker_rate_limiting_wait = 0
        #
        # when hitting error 429, add
        # increase rate limiting wait by this amount of seconds
        self.worker_rate_limiting_wait_step = 0.5

    def do_cmd_args(self):
        parser = argparse.ArgumentParser(description='test load target http endpoint')
        parser.add_argument('--conf', type=str,
                            help='location of config file')
        parser.add_argument('--debug', action='store_true',
                            help='print debug messages')
        parser.add_argument('--verbose', action='store_true',
                            help='print info messages')
        args = parser.parse_args()

        #
        # verbosity options
        #
        if args.debug:
            mylogger.setLevel(logging.DEBUG)
        elif args.verbose:
            mylogger.setLevel(logging.INFO)
        else:
            mylogger.setLevel(logging.ERROR)
        mylogger.debug("got config file: %s" % args.conf)

        #
        # file config options
        #
        if not os.path.exists(args.conf):
            raise Exception("Missing specified configuration: %s" % args.conf)
        self.confFile = args.conf

    def read_config(self):
        logging.debug("readConfig - started")
        try:
            with open(self.confFile, "r") as yamlConf:
                self.confData = yaml.load(yamlConf, Loader=yaml.FullLoader)
            # converting to minutes
            # for ease of flow control
            self.rpm = 60 * self.confData['targetRPS']
            self.path = self.confData['path']
            self.host = self.confData['serverURL']
            self.headers['X-Api-Key'] = self.confData['authKey']
            self.endPoint = "/".join([self.host, self.path])
            mylogger.debug("got header:%s" % self.headers)
            mylogger.debug("got host:%s" % self.host)
            mylogger.debug("got path:%s" % self.path)
            mylogger.debug("got rpm:%s" % self.rpm)
            mylogger.debug("got endPoint:%s" % self.endPoint)
        except Exception as e:
            mylogger.exception("config processing failed, can't continue")
            exit(1)
        logging.debug("readConfig - ended")

    def create_load(self,count):
        gmtime = time.gmtime()
        time_string = "%04d-%02d-%02d %02d:%02d:%02d" % (gmtime.tm_year, gmtime.tm_mon, gmtime.tm_mday,
                                           gmtime.tm_hour, gmtime.tm_min, gmtime.tm_sec)
        return {"name": "testbench", "date": time_string, "requests_sent": count}

    def run_test(self, start_count, reqs, end_time):
        # TODO: reqs_ok basic success stats that should be pushed back to parent
        reqs_ok = 0
        reqs_nok = 0

        # how many requests should be executed by this worker
        # before it got time limited
        max_count = start_count + reqs
        rate_limiting_wait = self.worker_rate_limiting_wait
        mylogger.debug("started with start_counter: %d, max_count: %d, end_time: %f" %(start_count, max_count, end_time))
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        while start_count < max_count:
            # time limiting
            # when responses are too slow, we can't finish all requests
            timeout = int(end_time - time.time())
            if timeout <= 0:
                mylogger.warning("reached running time limit, stop executing now. only finished %d out of %d requests"
                                 % (start_count, max_count))
                exit(1)
            load = self.create_load(start_count)
            mylogger.debug("worker pid: %d, start_count: %d, timeout: %d" % (os.getpid(), start_count, timeout))
            res = requests.post(self.endPoint, data=json.dumps(load), headers=self.headers,
                                verify=False, timeout=timeout)

            #
            # Error handling
            #
            if res.status_code == 429:
                reqs_ok += 1
                mylogger.warning("request rate is too high, slowing down ")
                rate_limiting_wait += self.worker_rate_limiting_wait_step
            elif res.status_code != 200 and res.status_code != 301:
                reqs_nok += 1
                mylogger.fatal("request failed with Error: %d" % res.status_code)
                mylogger.fatal(res.text)
                exit(1)

            try:
                resp = json.loads(res.text)
                if 'successful' not in resp:
                    reqs_nok += 1
                    mylogger.error("did not get successful response: %s " % res.text)
            except json.decoder.JSONDecodeError as e:
                mylogger.error("badly formatted response: %s" % res.text)

            start_count += 1
            time.sleep(rate_limiting_wait)

        mylogger.debug("finished")

    def gen_time_boundaries(self,boundaries):
        start_time = time.time()
        time_boundaries = list()
        for x in range(1, boundaries):
            time_boundaries.append(start_time + (x * self.flow_control_period_sec))
        mylogger.debug("generated time boundaries: %s" % (str(time_boundaries)))
        return time_boundaries


    def run(self):
        #
        # how many requests per adjustable periods
        # for example rpm 60 / 10 sec period will be 6 reqs per period
        #
        flow_control_period_reqs = int(tb.rpm / self.flow_control_period_sec)
        flow_control_periods = int(60 / self.flow_control_period_sec)

        # expected number of requests per worker during sampling
        reqs_per_worker = int(flow_control_period_reqs / self.max_workers)

        # attempt to distribute load over time
        thread_wait_before_start = int(self.warmup_time / self.max_workers)

        global_flow_control_period_counter = 0

        #
        # trucking request counters
        # allows to aproximate counter submited with request
        start_counter = 0

        #
        # run until Ctr-C interrupt got caught
        #
        while True:
            #
            # trying to distribute load evenly across threads and time
            # by sampling sub minute intervals
            for stop_time in tb.gen_time_boundaries(flow_control_periods):
                t0 = time.time()
                global_flow_control_period_counter += 1
                # reinit worker threads for each adjustable period
                all_workers = list()

                # generate list of workers
                for t in range(0, self.max_workers):
                    all_workers.append(Process(target=tb.run_test, args=(start_counter, reqs_per_worker, stop_time)))
                    start_counter += reqs_per_worker

                # started workers over self.warmup_time
                for t in all_workers:
                    time.sleep(thread_wait_before_start)
                    t.start()

                # waiting for threads to complete
                for t in all_workers:
                    t.join()

                # adjust load based on perfomance
                actual_stop_time = time.time()
                mylogger.debug("Period: %d - stop time: %d, actual_stop_time: %f" % (global_flow_control_period_counter, stop_time, actual_stop_time))
                if abs(stop_time - actual_stop_time) > self.flow_control_period_end_time_margin_sec:
                    if stop_time > actual_stop_time:
                        mylogger.debug("finished too fast, adjusting rate by waiting")
                        while time.time() < stop_time:
                            time.sleep(0.5)
                    elif stop_time < actual_stop_time:
                        mylogger.debug("load rate is too slow, need to adjust. somehow")
                    else:
                        mylogger.debug("target rate %d is achieved" % self.rpm)
                # calculating and printing adjusted rate
                t_diff = time.time() - t0
                rps = flow_control_period_reqs / t_diff
                print("Period: %d - finished %d requests in %f sec (%f rps)" %(global_flow_control_period_counter,
                                                      flow_control_period_reqs,t_diff, rps))
                self.summary_total_reqs += self.summary_total_reqs + flow_control_period_reqs
                if self.summary_avg_rps == 0.0:
                    self.summary_avg_rps = rps
                else:
                    self.summary_avg_rps = (self.summary_avg_rps + rps ) / 2


if __name__ == "__main__":
    try:
        tb = TestBench()
        tb.do_cmd_args()
        tb.read_config()
        tb.run()
    except KeyboardInterrupt as e:
        print("Exiting...")
        print("Summary: total requests: %d, avg rps: %f"
              % (tb.summary_total_reqs,tb.summary_avg_rps))
        exit(0)
